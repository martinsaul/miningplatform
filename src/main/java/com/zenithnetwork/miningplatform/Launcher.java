package com.zenithnetwork.miningplatform;

import com.zenithnetwork.miningplatform.misc.WorkingDirectory;
import com.zenithnetwork.miningplatform.object.Game;
import com.zenithnetwork.miningplatform.object.GameInstance;
import com.zenithnetwork.miningplatform.process.ProcessHost;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class Launcher {

    public static void main(String[] args){
        //SpringApplication.run(Launcher.class, args);

        //This is just dummy testing stuff.
        try {
            //noinspection ResultOfMethodCallIgnored
            WorkingDirectory.get("world/").mkdir();
            WorkingDirectory.get("world/test_1/").mkdir();
            GameInstance gameInstance = new GameInstance(new Game.Minecraft(), WorkingDirectory.get("world/test_1"));

            gameInstance.start();
            ProcessHost ph = gameInstance.getProcess();

            Thread.sleep(60000);
            ph.runCommand("stop");
            ph.waitForCompletion();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

}

package com.zenithnetwork.miningplatform.object;

import com.zenithnetwork.miningplatform.misc.WorkingDirectory;

import java.io.File;

public class Game {

    private ExecutableType executableType;
    private File executable;
    private String staticParameters;
    private String name;

    public Game(ExecutableType executableType, File executable, String staticParameters, String name) {
        this.executableType = executableType;
        this.executable = executable;
        this.staticParameters = staticParameters;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return executableType.getCommand() + executable.getAbsolutePath() + " " + staticParameters;
    }

    public static class Minecraft extends Game{
        public Minecraft() {
            super(ExecutableType.Java, WorkingDirectory.get("/server/server.jar"), "nogui", "Minecraft");
        }
    }
}

package com.zenithnetwork.miningplatform.object;

public enum ExecutableType {
    Java("java -jar "), WindowsExe("");

    private final String command;

    ExecutableType(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}

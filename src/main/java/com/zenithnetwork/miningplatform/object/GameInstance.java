package com.zenithnetwork.miningplatform.object;

import com.zenithnetwork.miningplatform.process.ProcessHost;
import com.zenithnetwork.miningplatform.process.listener.ProcessListener;
import org.apache.tomcat.jni.Proc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class GameInstance {

    private final static Logger log = LoggerFactory.getLogger(GameInstance.class);

    private static int sid = 1;
    private final int id;
    private final Game game;
    private final File workingDirectory;
    private final String name;
    private ProcessHost processHost;
    private final ProcessListener listener;

    public GameInstance(Game game, File workingDirectory) {
        this.id = sid;
        sid++;
        this.game = game;
        this.workingDirectory = workingDirectory;
        this.name = game.getName() + workingDirectory.getName() + id;
        listener = new ProcessListener() {
            @Override
            public void update(String take) {
                log.info(getInstanceName() + " - " + take);
            }
        };
    }

    public void start() throws IOException {
        processHost = new ProcessHost(game.getName(), listener, game.getCommand(), workingDirectory);
    }

    public String getInstanceName(){
        return name;
    }

    public ProcessHost getProcess() {
        return processHost;
    }
}

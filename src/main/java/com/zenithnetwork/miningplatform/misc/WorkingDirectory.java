package com.zenithnetwork.miningplatform.misc;

import java.io.File;

public class WorkingDirectory {

	private static final File workingDirectory = new File(".").getAbsoluteFile().getParentFile();

	public static File get(String path) {
		return new File(workingDirectory, path);
	}
}

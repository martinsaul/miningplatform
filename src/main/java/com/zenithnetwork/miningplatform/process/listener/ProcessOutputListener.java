package com.zenithnetwork.miningplatform.process.listener;

import com.zenithnetwork.miningplatform.thread.KillableRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

public class ProcessOutputListener extends KillableRunnable {
	private final static Logger log = LoggerFactory.getLogger(ProcessOutputListener.class);

	private final LinkedBlockingQueue<String> input = new LinkedBlockingQueue<>();
	private final ProcessListener listener;

	public ProcessOutputListener(ProcessListener listener){
		super(listener.toString() + "-Output");
		this.listener = listener;
	}

	public void run(){
		while (isAlive()){
			try {
				listener.update(input.take());
			} catch (InterruptedException exp) {
				if(isAlive())
					log.warn("Received interruption when listener is still alive. Going back into listening.", exp);
			}
		}
	}

	public void notify(String line){
		try {
			input.offer(line);
		} catch (NullPointerException ignore) {}
	}
}

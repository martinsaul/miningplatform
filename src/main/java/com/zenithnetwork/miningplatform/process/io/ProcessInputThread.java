package com.zenithnetwork.miningplatform.process.io;

import com.zenithnetwork.miningplatform.process.ProcessHost;
import com.zenithnetwork.miningplatform.thread.KillableRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.concurrent.LinkedBlockingQueue;

public class ProcessInputThread extends KillableRunnable {

    private final static Logger log = LoggerFactory.getLogger(ProcessInputThread.class);

    private final BufferedWriter writer;
    private final ProcessHost processHost;
    private final LinkedBlockingQueue<String> input = new LinkedBlockingQueue<>();

    public ProcessInputThread(ProcessHost processHost) {
        super(processHost.getName() + "-Input");
        this.writer = new BufferedWriter(new OutputStreamWriter(processHost.getProcess().getOutputStream()));
        this.processHost = processHost;
    }

    public void post(String command) {
        input.offer(command);
    }

    @Override
    public void run() {
        try {
            while (processHost.getProcess().isAlive()) {
                String command = input.take();
                log.info("Pushing command: " + command);
                writer.write(command);
                writer.newLine();
                writer.flush();
                log.info("Push complete.");
            }
        } catch (Exception p) { //TODO }
        }
    }
}

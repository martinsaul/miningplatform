package com.zenithnetwork.miningplatform.process.io;

import com.zenithnetwork.miningplatform.process.ProcessHost;
import com.zenithnetwork.miningplatform.process.listener.ProcessListener;
import com.zenithnetwork.miningplatform.process.listener.ProcessOutputListener;
import com.zenithnetwork.miningplatform.thread.KillableRunnable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProcessOutputThread extends KillableRunnable {
    private final BufferedReader reader;
    private final ProcessOutputListener listener;
    private final ProcessHost processHost;

    public ProcessOutputThread(ProcessListener listener, ProcessHost processHost) {
        super(processHost.getName() + "-Output");
        this.listener = new ProcessOutputListener(listener);
        this.processHost = processHost;
        this.reader = new BufferedReader(new InputStreamReader(processHost.getProcess().getInputStream()));
    }

    @Override
    public void run() {
        thread(this.listener).start();
        while (processHost.getProcess().isAlive()) {
            try {
                listener.notify(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.zenithnetwork.miningplatform.process;

import com.zenithnetwork.miningplatform.object.ExecutableType;
import com.zenithnetwork.miningplatform.process.io.ProcessInputThread;
import com.zenithnetwork.miningplatform.process.io.ProcessOutputThread;
import com.zenithnetwork.miningplatform.thread.ThreadHost;
import com.zenithnetwork.miningplatform.process.listener.ProcessListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class ProcessHost extends ThreadHost implements Runnable {

    private final static Logger log = LoggerFactory.getLogger(ProcessHost.class);

    private static int sid = 1;

    private final int id;
    private final Process process;
    private final ProcessOutputThread output;
    private final ProcessInputThread input;
    private final String name;

    public ProcessHost(String name, ProcessListener listener, String command, File workingDir) throws IOException {
        this.name = name;
        this.id = sid;
        sid++;
        log.info("Process host is starting application and I/O monitoring.");
        this.process = Runtime.getRuntime().exec(command, new String[0], workingDir);
        this.input = new ProcessInputThread(this);
        this.output = new ProcessOutputThread(listener, this);
        thread(output).start();
        thread(input).start();
        new Thread(this).start();
        log.info("Process host has finished initializing.");
    }

    public void runCommand(String command){
        input.post(command);
    }

    @Override
    public void run() {
        try {
            process.waitFor();
            int exitValue = process.exitValue();
            if(exitValue == 0) log.info("Process has terminated peacefully.");
            else log.error("Process has terminated with exit code: " + exitValue);
            kill();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Process getProcess() {
        return process;
    }

    public String getName(){
        return name+"-"+id;
    }
}

package com.zenithnetwork.miningplatform.thread;

public abstract class KillableRunnable implements Runnable{
    private boolean alive;
    private ThreadHost host;
    private final String name;
    private final int id;
    private static int sid = 1;

    public KillableRunnable(String threadName){
        alive = true;
        host = null;
        this.id = sid;
        this.name = threadName + "-"+id;
        sid++;
    }

    void setHost(ThreadHost host){
        if(this.host != null)
            throw new RuntimeException("This thread is already hosted!");
        this.host = host;
    }

    public Thread thread(KillableRunnable runnable){
        return host.thread(runnable);
    }
    public void kill(){
        alive = false;
    }

    protected boolean isAlive(){
        return alive;
    }

    public String getName() {
        return name;
    }
}

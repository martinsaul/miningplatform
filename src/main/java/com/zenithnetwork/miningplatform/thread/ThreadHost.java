package com.zenithnetwork.miningplatform.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public abstract class ThreadHost {
    private final static Logger log = LoggerFactory.getLogger(ThreadHost.class);

    private LinkedBlockingDeque<Thread> threadQueue;
    private HashMap<KillableRunnable, Thread> threadMap;
    private boolean alive;

    protected ThreadHost(){
        threadMap = new HashMap<>();
        threadQueue = new LinkedBlockingDeque<>();
        alive = true;
    }

    protected Thread thread(KillableRunnable runnable){
        if(!alive)
            throw new RuntimeException("Thread Host has already been killed and cannot accept new threads to spawn.");
        runnable.setHost(this);
        Thread newThread = new Thread(runnable, runnable.getName());
        threadMap.put(runnable, newThread);
        threadQueue.add(newThread);
        return newThread;
    }

    public void kill(){
        alive = false;
        for(KillableRunnable runnable: threadMap.keySet()){
            runnable.kill();
            threadMap.get(runnable).interrupt();
        }
    }

    public void waitForCompletion() throws InterruptedException {
        Thread thread;
        while ((thread = threadQueue.poll(2, TimeUnit.SECONDS)) != null) {
            try {
                log.debug("Waiting for " + thread);
                thread.join();
                log.debug("Finish waiting for " + thread);
            } catch(InterruptedException e) {
                log.error("Got interrupted on joining thread: " + thread);
                throw e;
            }
        }
        log.debug("Concluded terminating Thread Host");
    }
}
